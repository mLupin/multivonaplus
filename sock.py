import time
import os
import socket
import select
import sys
import datetime
import threading
import traceback
import logging
from sys import platform
if platform == "linux" or platform == "linux2":
	import prctl
from classes import ProgramState
from config import Cfg, Enviroment

class SocketThread(threading.Thread):
	ps = None
	forceStop = False
	s = None

	def __init__(self, ps):
		super(SocketThread, self).__init__(name="SocketClient%sThread" % ps.slave_id)
		logging.info("Socket Client %s Thread launching" % ps.slave_id)
		self.ps = ps

	def run(self):
		if platform == "linux" or platform == "linux2":
			prctl.set_name("SocketClient%sThread" % self.ps.slave_id)
		self.SocketClientStart()

	def SocketClientStart(self):
		try:
			logging.info("Socket Client starting")

			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			self.s.connect((self.ps.server_addr, int(self.ps.server_port)))
			self.client_loop()
		except:
			if Cfg.ENV.value <= Enviroment.Beta.value:
				logging.warning("Socket Client interrupted. Relaunching…")
				self.SocketClientStart()
			else:
				logging.error("Socket Client interrupted. Not relaunching in dev env.")
				traceback.print_exc()


	def client_loop(self):
		try:
			isRunning = False
			while self.forceStop != True:
				if isRunning == False:
					data = self.s.recv(65535)
					if data:
						text = data.decode('utf-8')
						logging.info("Received %d bytes from the server: %s" % (len(text), text))
						self.ps.tts.say(text)
		finally:
			self.s.close()

			