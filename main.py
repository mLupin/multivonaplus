#!/usr/bin/python3
import time
import os
import sys
import datetime
import atexit
import threading
import traceback
import logging
from sys import platform
if platform == "linux" or platform == "linux2":
	import prctl
from tm import ThreadManager
from config import Cfg, Enviroment
from classes import ProgramState

program_state = ProgramState()
statusLED = None

def start():
	global program_state
	logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)-8s] %(threadName)-23s - %(message)s')
	logging.info("Client started as MASTER")

	if len(sys.argv) < 3:
		logging.error("Server address and port not specified, exiting")
		sys.exit(137)
		return

	program_state.server_addr = sys.argv[1]
	program_state.server_port = sys.argv[2]

	tm = ThreadManager(program_state)
	tm.begin()

def main():
	if platform == "linux" or platform == "linux2":
		prctl.set_name("MainThread")
	try:
		start()
	except KeyboardInterrupt:
		logging.info("Termination signal from user")
		for sock in program_state.socks_to_close:
			try:
				sock.close()
			except:
				pass
		try:
			sys.exit(254)
		except SystemExit:
			os._exit(255)
	except:
		if Cfg.ENV.value <= Enviroment.Beta.value:
			logging.warning("Program interrupted. Relaunching…")
			main()
		else:
			logging.error("Program interrupted. Not relaunching in dev env.")
			traceback.print_exc()
		
@atexit.register
def onExit():
	if Cfg.ENV.value <= Enviroment.Beta.value:
		logging.warning("Program terminated. Relaunching…")
		main()
	else:
		logging.error("Program terminated. Not relaunching in dev env.")

if __name__ == '__main__':
	main()