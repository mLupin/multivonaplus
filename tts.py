import time
import os
import sys
import datetime
import threading
import logging
import traceback
from sys import platform
if platform == "linux" or platform == "linux2":
	import prctl
import pyvona
import hashlib
import pygame
from pathlib import Path
from config import Cfg, Enviroment

class TTSThread(threading.Thread):
	ps = None
	forceStop = False
	v = None

	def __init__(self, ps):
		super(TTSThread, self).__init__(name="TTSThread")
		logging.info("TTS Thread launching")
		self.ps = ps
		self.v = pyvona.create_voice(Cfg.IVONA_KEY, Cfg.IVONA_SECRET)

	def run(self):
		if platform == "linux" or platform == "linux2":
			prctl.set_name("TTSThread")
		self.vrStart()

	def vrStart(self):
		try:
			logging.info("TTS starting")
			self.recognize_forever()
		except:
			if Cfg.ENV.value <= Enviroment.Beta.value:
				logging.warning("TTS interrupted. Relaunching…")
				self.vrStart()
			else:
				logging.error("TTS interrupted. Not relaunching in dev env.")
				traceback.print_exc()

	def recognize_forever(self):
		isRunning = False
		while self.forceStop != True:
			if isRunning == False:
				logging.info("TTS started")
				isRunning = True

			time.sleep(3)
			
	def say(self, text, rate="medium", voice="Maja", sentence_break=500):
		self.v.sentence_break = sentence_break
		self.v.voice_name = voice
		self.v.speech_rate = rate
		self.v.codec = "ogg"
		self.v.region = "eu-west"
		textHash = hashlib.md5(text.encode('utf-8')).hexdigest()
		if self.speechFileExists("%s_%s_%s_%d.ogg" % (textHash, voice, rate, sentence_break)):
			logging.debug("Playing voice from cache")
		else:
			logging.debug("Voice not cached, downloading")
			self.v.fetch_voice(text, "%s/speech/%s_%s_%s_%d" % (os.getcwd(), textHash, voice, rate, sentence_break))

			if not self.speechFileExists("%s_%s_%s_%d.ogg" % (textHash, voice, rate, sentence_break)):
				self.say("Błąd połączenia z serwerem syntezatora mowy")
				return


		self.play("%s_%s_%s_%d.ogg" % (textHash, voice, rate, sentence_break), "speech")

	def speechFileExists(self, path):
		if Path("%s/speech/%s" % (os.getcwd(), path)).is_file():
			return True
		else:
			return False

	def play(self, name, folder="sounds"):
		if not pygame.mixer.get_init():
			pygame.mixer.init()
			channel = pygame.mixer.Channel(5)
		else:
			channel = pygame.mixer.find_channel()
			if channel is None:
				pygame.mixer.set_num_channels(pygame.mixer.get_num_channels()+1)
				channel = pygame.mixer.find_channel()

		sound = pygame.mixer.Sound("%s/%s/%s" % (os.getcwd(), folder, name))
		channel.play(sound)
		while channel.get_busy():
			pass
